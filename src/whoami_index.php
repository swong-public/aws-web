<!DOCTYPE html> 
<?php

  $IP = getHostByName(getHostName());
	$HOSTNAME = gethostname();
	$REFRESH = 10000; # 10 seconds

	$WHOAMI = shell_exec ('./genname.sh ' . '  2>&1');

  if(isset($_GET["refresh"])) {
    $IN_REFRESH = $_GET["refresh"] * 1000;
		if ( $IN_REFRESH > 0 ) {
			$REFRESH=$IN_REFRESH;
		}
  }

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>aws-web - whoami</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link type="text/css" rel="stylesheet" href="default.css"/>
    <style>
      body { text-align: center; padding: 150px; }
      h1 { font-size: 50px; }
      body { font: 20px Helvetica, sans-serif; color: #333; }
      article { display: block; text-align: left; width: 650px; margin: 0 auto; }
      a { color: #dc8100; text-decoration: none; }
      a:hover { color: #333; text-decoration: none; }
    </style>
		<script type="text/javascript" src="/default.js"></script>
		<script>
			 var time = new Date().getTime();
			 $(document.body).bind("mousemove keypress", function(e) {
					 time = new Date().getTime();
			 });
		</script>
</head>
<body onload=<?php echo "display_timestamp(${REFRESH});"; ?> >
<form method="GET">
<div id="page" style="padding:5px;clear:both;border:1px solid #B0B0B0; overflow:hidden; position: relative" ><!-- page -->
<div id="content" style="float:left;clear:both; width: 100%; border:0px solid red;position: relative"><!-- content -->

  <article>
      <h3><span id='timestamp' ></span></h3>
      <div>
          <p><label style="float: left; width: 500px; margin-right: 10px; text-align: left; font-weight: bold; clear: left;" ><?php echo "I am " . $WHOAMI . " on " .  $HOSTNAME . " (" . $IP . ")"; ?></label></p>
					<p><label style="float: left; width: 500px; margin-right: 10px; text-align: left; font-weight: normal; clear: left;" >This page will refresh in <?php $REFRESH = $REFRESH / 1000; echo "$REFRESH" ; ?> seconds.</label></p> 
  				<p style="float:left"><label for="ip" style="float:left; width: 150px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Refresh (secs):</label><input size="5" name="refresh" type="text" /></p>
  				<p style="float:left; margin-left:5px "><input size="5" type="submit" value="refresh" /></p>

      </div>
  </article>

</div><!-- content -->
</div><!-- page -->
</form>
</body></html>



import boto.ec2
from iniparse import INIConfig, RawConfigParser

from time import gmtime, localtime, strftime
import os
import time
import sys
debug = False

# Status: WIP

class Aws ():
	user = ""
	key = ""
	secret = ""

	def __init__(self, user=None):
		self.user = user
		self.key, self.secret = self.aws_credentials(self.user)
	
	def boto_config_path(self,user=None):
			"""
			Given an user name, return the path to the corresponding boto
			configuration file. If no user given, return the default config file.
			"""
			path = '~/.boto' + ('_' + user if user else '')
			clean_path = os.path.abspath(os.path.expanduser(path))
			if not os.path.isfile(clean_path):
				path = '/etc/boto.cfg'
				clean_path = os.path.abspath(os.path.expanduser(path))
			if os.path.isfile(clean_path):
					return clean_path
			else:
					errmsg = "cannot find boto config file {} for {}".format(clean_path, user)
					raise ValueError(errmsg)

	def aws_credentials(self,user=None):
		global debug
		"""
		Return a tuple of AWS credentials (access key id and secret access key) for
		the given user.
		"""
		key = ''
		secret = ''
		try:
			cfg = INIConfig(open(self.boto_config_path(user)))
			try:
				key = cfg[user]['aws_access_key_id']
				secret = cfg[user]['aws_secret_access_key']
				if debug:
					print "returning key=%s secret=%s for user %s" % (key, secret, user) 
				return ( key, secret )
			except Exception, e:
				print e 
		except Exception, e:
			print  e 
#			raise

	def info(self):
		return ( 'user: ' + self.user + ' key: ' + self.key + ' secret: ' + self.secret )

#conn = EC2Connection(*aws_credentials('razorassoc'))

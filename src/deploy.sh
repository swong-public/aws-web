#!/bin/sh

CHECK_STRING="80->80"

check_instances () {
  if [ "${CHECK_STRING}" = "*" ]
  then
    echo "removing  all containers ..."
    docker rm -f `docker ps | cut -d" " -f1 | sed 's/CONTAINER//g' | xargs`
    return
  fi
  CONTAINER="`docker ps | grep ${CHECK_STRING} | cut -d" " -f1`"
  if [ "${CONTAINER}" != "" ]
  then
    echo "removing  ${IMAGE_ID} with container ${CONTAINER} ..."
    docker rm -f ${CONTAINER}
    sleep 1
  fi
}



docker pull registry.gitlab.com/swong-public/docker/aws-web:latest
check_instances
docker run -d -p 80 registry.gitlab.com/swong-public/docker/aws-web:latest



#!/bin/sh
PROGNAME="`basename $0`"
USAGE="${PROGNAME} [image]"
IMAGE=aws-web

usage () {

cat<<EOT:

  Usage:${USAGE}
	image: default ${IMAGE}
  eg:
    ${PROGNAME} aws-web:v1

EOT:

}

if [ "${1}" != "" -a "${1}" != "-h" ]
then
	IMAGE="$1"
fi

if [ "${IMAGE}" = "" -o "${1}" = "-h" ]
then
	usage
	exit
fi

echo docker build -t ${IMAGE} .
docker build -t ${IMAGE} .
./squash_build.sh ${IMAGE}

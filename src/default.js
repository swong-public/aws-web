function setPage()
{
	// nothing
}


function set_countdown(in_refresh){
  var refresh=10000; // Refresh rate in milli seconds
  if ( in_refresh && typeof(in_refresh) == "number" ) {
    refresh = in_refresh;
  }
  mytime=setTimeout(display_timestamp,refresh,refresh)
}

// setup a countdown to display a timestamp into a html element with id call timestamp.
// eg. use : <h3><span id='timestamp'></span></h3>
//           in body: onload=display_timestamp();>
// refresh is in milliseconds
// initialise time in head:
//    <script>
//       var time = new Date().getTime();
//       $(document.body).bind("mousemove keypress", function(e) {
//           time = new Date().getTime();
//       });
//    </script>
function display_timestamp(refresh) {
  var strcount
  var x = new Date()
  document.getElementById('timestamp').innerHTML = x;
 
  if(new Date().getTime() - time >= refresh) {
    window.location.reload(true);
  } else  {
    set_countdown(refresh);
  }
}


#!/bin/sh
#
# This script flattens a image base on a image id or the given image-tag
#
#
PROGNAME="`basename $0`"
USAGE="${PROGNAME} [-h] [-id image_id ] tag"
IMAGE_ID=
IMAGE=
TMPFILE=/tmp/${PROGNAME}_tmp_??
EXPORTED=./exported_image


trap "{ 
    rm -f ${TMPFILE}; 
		clean_up
  }" 0 1 2 5 15 EXIT

clean_up() {
  echo cleaning up
  docker rmi ${IMAGE_IMPORT} 2>/dev/null
  rm -f Dockerfile.imported
  rm -f ${EXPORTED}
}

usage () {
  cat<<EOT:

  Usage: ${USAGE}
  -id     -  id of image to be flatten e.g. -id e7396e198b87
  tag     -  e.g. registry.gitlab.com/swong-public/docker/aws-web

EOT:

}

gen_import_dockerfile () {

	cat<<EOT: > Dockerfile.imported
FROM ${IMAGE_IMPORT}


RUN rm -rf /var/run/dnsmasq.pid /var/run/httpd/*
RUN sed -i 's/#ServerName.*/ServerName 0.0.0.0:80/g' /etc/httpd/conf/httpd.conf
ADD services.sh /


EXPOSE 80 443 22
CMD /services.sh

EOT:

}

get_imported_image_name () {

	IMAGE_IMPORT="`echo ${IMAGE} | sed 's/:.*/:imported/g'`"

	rc="`echo ${IMAGE_IMPORT} | grep ':'`"
	if [ "${rc}" = "" ]
	then
			IMAGE_IMPORT="${IMAGE_IMPORT}:imported"
	fi

}

ARGS=
ALL_ARGS=
#---------------------------------------------------------------------------
# Get the command line arguments
# Remaining args are left in ARGS
#---------------------------------------------------------------------------
getopts () {
    while [ $# != 0 ]
    do
        case $1 in
        -h ) HELP=1; usage; exit;
             ;;
        -id ) shift; IMAGE_ID="${1}"; shift;
            ;;
        * ) ARGS="${ARGS} $1"; shift
            ;;
        esac
    done
}


ALL_ARGS="$@"
getopts "$@"
IMAGE="${ARGS}"


if [ "${IMAGE_ID}" = "" ]
then
  IMAGE_ID=${IMAGE}
fi

if [ "${IMAGE}" = "" ]
then
	usage
	exit 1
fi


	

container="`docker run -d ${IMAGE_ID}`"
echo "`hostname -s` exporting to ${EXPORTED} .. "

get_imported_image_name
docker export ${container} >${EXPORTED}
docker rm -f ${container}
echo "`hostname -s` importing into $IMAGE_IMPORT .."
docker import ${EXPORTED} ${IMAGE_IMPORT}
gen_import_dockerfile 
echo "`hostname -s` rebuilding squashed image $IMAGE .."
docker build --force-rm -f Dockerfile.imported -t ${IMAGE} .







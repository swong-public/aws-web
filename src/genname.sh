#!/bin/sh
NAME_FILE=node_name
arr[0]="medusa"
arr[1]="harpies"
arr[2]="centaur"
arr[3]="cyclopes"
arr[4]="chimaera"
arr[5]="cerberus"
arr[6]="arachne"
arr[7]="minotaur"
arr[8]="pegasus"
arr[9]="polyphemus"
arr[10]="scylla"
arr[11]="sirens"
arr[12]="orthrus"
arr[13]="sphinx"
arr[14]="hydra"
arr[15]="ladon"
arr[16]="echina"
arr[17]="poseidon"
arr[18]="athos"
arr[19]="cetus"
rand=$[$RANDOM % 20]
#echo $(date)
SECS=`date '+%S'`

if [ ! -f ${NAME_FILE} ]
then
	name=`echo ${arr[${rand}]}-${SECS}`
	echo ${name} > ${NAME_FILE}	
fi
cat ${NAME_FILE}




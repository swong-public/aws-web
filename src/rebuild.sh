#!/bin/sh
PROGNAME="`basename $0`"
USAGE="${PROGNAME} [image]"
IMAGE=aws-web

usage () {

cat<<EOT:

  Usage:${USAGE}
	image: default ${IMAGE}
  eg:
    ${PROGNAME} aws-web:v1

EOT:

}

if [ "${1}" != "" -a "${1}" != "-h" ]
then
	IMAGE="$1"
fi

if [ "${IMAGE}" = "" -o "${1}" = "-h" ]
then
	usage
	exit
fi

CONTAINER="`docker ps | grep ${IMAGE} | cut -d" " -f1`"

if [ "${CONTAINER}" != "" ]
then
  echo "removing  ${IMAGE} with container ${CONTAINER} ..."
  docker rm -f ${CONTAINER}
  sleep 1
fi

echo building: docker build -t ${IMAGE} .
docker build -t ${IMAGE} .
./squash_build.sh

echo "starting up  ${IMAGE} ..."
docker run -d -p 80:80 ${IMAGE}




<!DOCTYPE html> 
<?php

  $version = "";
  $MAX_INSTANCES = 3;
  $DEBUG = 0;
  $keyid = "";
  $secret = "";
  $qty = "0";
  $terminate_instance = "";
  $stop_instance = "";
  $ids = "";
  $ids_array = array('');
  $status_msg = "";
  $status_array = array('');
  $inventory_msg = "";
  $inventory_array = array('');
  $ami_list = array('ami-162c2575 - amazon linux','ami-98aea6fb - centos7.3', 'ami-79000a1a - ubuntu');
  $ami = "Default";
  $action = "";
  $rc = 3;
  $launch_name = "";
  $launch_tag = "";
  $stop_name = "";
  $stop_tag = "";
  $terminate_name = "";
  $terminate_tag = "";
  $launch_group = "";
  $start_group = "";
  $stop_group = "";
  $terminate_group = "";
  $debug_str = "";
  $in_user = "";
  $in_profile = "";

//  $ping_rc = shell_exec ('ping -c 1 google.com ' . ' 2>&1');
//  echo "ping_rc=" . $ping_rc . "<br>";
 
  function debug ($msg) {
    global $debug_str;
    global $DEBUG;
    if ( $DEBUG != 1 ) { 
      return '';
    }
    $debug_str = $debug_str . 'DEBUG: ' . $msg . '<br>';
  }

  function get_version () {
      global $version;
      $fh = fopen("version" ,'r');
      if ($fh != null) {
        $version = fread($fh,filesize("version"));
       }
  }

  function update_ids_array ($raw_line_array) {
      global $ids_array;
      global $debug_str;
      debug ('update_ids_array():');
      if ( $raw_line_array == null ) {
        return "";
      }
      foreach ( $raw_line_array as $line ) {
        $list = explode(';', $line);
        foreach ($list as $pair_str ) {
          $pair = explode(':', $pair_str);
          if ( trim($pair[0]) == 'id' ) {
            $ids_array[] = $pair[1];
          } else {
            continue;
          }
        }
      }
  }


  function updateInventory () {
      global $keyid, $secret, $rc; 
      global $ids, $ids_array, $status_msg, $status_array, $inventory_msg, $inventory_array;
      global $debug_str;
      global $in_profile;
      if ( $in_profile != "" ) {
        exec ('./i_inventory.py -u ' . $in_profile . ' 2>&1', $inventory_array, $rc);
      } else {
        exec ('./i_inventory.py -k ' . $keyid . ' -s ' . $secret . ' 2>&1', $inventory_array, $rc);
      }
      if ( $rc == 0 ) {
        if ( $in_profile != "" ) {
          $ids = shell_exec ('./i_inventory.py -l -u ' . $in_profile . ' 2>&1');
        } else {
          $ids = shell_exec ('./i_inventory.py -l -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
        }
        $raw_line_array = split ("\n", $ids);
        update_ids_array ($raw_line_array);
        $status_msg = $status_msg . "Inventory updated ok.\n";
        $status_array = split ("\n", $status_msg);
      } else {
        if ( $in_profile != "" ) {
          $status_msg = shell_exec ('./i_inventory.py -l -u ' . $in_profile . ' 2>&1');
        } else {
          $status_msg = shell_exec ('./i_inventory.py -l -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
        }
        $status_msg = "Inventory update failed.\n" . $status_msg;
        $status_array = split ("\n", $status_msg) + $inventory_array;
        $inventory_array = array('');
      }
  } // updateInventory ()

  // instring: [ key1:value1 ; key1:value2 .. ]
  function convert_2_array ($instring) {
    global $debug_str;
    $tags_flag = 0;
    $instring = str_replace(array("\r", "\n"), '', $instring);
#   $debug_str = 'instring: ' . $instring . " -- <br>";
    if (!$instring) {
      return(null);
    }
    $ret_array = array("user" => "-", "Name" => "-", "Group" => "-" , "TAGS" => "-");
    $list = explode(';', $instring);
    foreach ($list as $pair_str ) {
      $pair = explode(':', $pair_str);
      if ( trim($pair[0]) == 'TAGS' ) {
        $tags_flag = 1;
        $ret_array["TAGS"] = "";
        continue;
      }
      if ( $pair[0] ) {
//        $debug_str = $debug_str . 'pair[0]: ' . $pair[0] . ' pair[1]: ' . $pair[1] . ";";
//        $ret_array[$pair[0]] = $pair[1];
        if ($tags_flag == 1) {
          if ( $pair[0] != "Name" && $pair[0] != "Group" && $pair[0] != "user" ) {
            debug ( 'convert_2_array(): new tag: ' . $pair[0] );
            $ret_array["TAGS"] = $ret_array["TAGS"] . ' ' . trim($pair[0]) . ':' . trim($pair[1]);
          } else {
            $ret_array = array_merge($ret_array, array( trim($pair[0]) => trim($pair[1]) ));
          }
        } else {
          if ( $pair[1] ) {
            $ret_array = array_merge($ret_array, array( trim($pair[0]) => trim($pair[1]) ));
          } else {
            $ret_array = array_merge($ret_array, array( trim($pair[0]) => 'null' ));
          }
        }
      }
    }
    //$debug_str = $debug_str . ' ret_array=' . var_dump($ret_array) . "<br>";
    $debug_str = $debug_str . "<br>";
    return $ret_array;
  }

  foreach ( $_POST as $key => $value ) {
//    $debug_str = $debug_str . 'key: ' . $key . ' val: ' . $value . "\n";
    if ( $key == 'key' ) {
      $keyid=$value;
    }
    if ( $key == 'secret' ) {
      $secret=$value;
    }
    if ( $key == 'qty' ) {
      if ( $value != "" ) {
        $qty=$value;
      }
    }
    if ( $key == 'in_user' ) {
      $in_user=$value;
    }
    if ( $key == 'terminate_instance' ) {
      $terminate_instance=$value;
    }
    if ( $key == 'start_instance' ) {
      $start_instance=$value;
    }
    if ( $key == 'stop_instance' ) {
      $stop_instance=$value;
    }
    if ( $key == 'launch_name' ) {
      $launch_name=$value;
    }
    if ( $key == 'launch_tag' ) {
      $launch_tag=$value;
    }
    if ( $key == 'launch_group' ) {
      $launch_group=$value;
    }
    if ( $key == 'start_name' ) {
      $start_name=$value;
    }
    if ( $key == 'start_tag' ) {
      $start_tag=$value;
    }
    if ( $key == 'start_group' ) {
      $start_group=$value;
    }
    if ( $key == 'stop_name' ) {
      $stop_name=$value;
    }
    if ( $key == 'stop_tag' ) {
      $stop_tag=$value;
    }
    if ( $key == 'stop_group' ) {
      $stop_group=$value;
    }
    if ( $key == 'terminate_name' ) {
      $terminate_name=$value;
    }
    if ( $key == 'terminate_tag' ) {
      $terminate_tag=$value;
    }
    if ( $key == 'terminate_group' ) {
      $terminate_group=$value;
    }
    if ( $key == 'ami' ) {
      $ami=$value;
    }
  }

  if(isset($_POST["submit_btn"])) {
    $action = $_POST["submit_btn"];
  }

  switch ($action) {

    case "launch":
      if ( $qty > $MAX_INSTANCES ) {
        $status_msg = "ERROR! launch aborted - instances to launch " . $qty . " >  max allowed of " . $MAX_INSTANCES . "\n";
      } else {
        $in_tags = "";
        if ($in_user != ""  ){
          $in_tags = 'user:' . $in_user;
				}
        if ($launch_name != ""  ){
					if ($in_tags == "" ) $in_tags = 'Name:' . $launch_name;
					else	$in_tags = $in_tags . ',' . 'Name:' . $launch_name;
        }
        if ($launch_group != ""  ){
          if ($in_tags == "" ) $in_tags = 'Group:' . $launch_group;
					else $in_tags = $in_tags . ',' . 'Group:' . $launch_group;
        }
        if ($launch_tag != ""  ){
           if ($in_tags == "" ) $in_tags = $launch_tag;
          else $in_tags = $in_tags . ',' . $launch_tag;
        }
        if ( $ami == "" ) {
          $status_msg = shell_exec ('./i_inventory.py -C launch -q ' . $qty . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
        } else {
          $status_msg = shell_exec ('./i_inventory.py -C launch -q ' . $qty . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' -a "' . $ami . '" 2>&1');
        }
      }
      $status_array = split ("\n", $status_msg);
      updateInventory();
      break;

    case "terminate":
      $in_tags = "";
      if ($terminate_name != ""  ){
        $in_tags = 'Name:' . $terminate_name;
      }
      if ($terminate_group != ""  ){
				if ( $in_tags != "" ) $in_tags = $in_tags . ',' . 'Group:' . $terminate_group;
				else $in_tags = 'Group:' . $terminate_group;
      }
      if ($terminate_tag != ""  ){
				if ( $in_tags != "" ) $in_tags = $in_tags . ',' . $terminate_tag;
        else $in_tags = $terminate_tag;
      }
      if ( $terminate_instance == "" ) {
        $status_msg = shell_exec ('./i_inventory.py -C terminate -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      } else {
        $status_msg = shell_exec ('./i_inventory.py -C terminate -i ' . $terminate_instance . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      }
      $status_array = split ("\n", $status_msg);
      updateInventory();
      break;


    case "stop":
      $in_tags = "";
      if ($stop_name != ""  ){
        $in_tags = 'Name:' . $stop_name;
      }
      if ($stop_group != ""  ){
				if ( $in_tags != "" ) $in_tags = $in_tags . ',' . 'Group:' . $stop_group;
				else $in_tags = 'Group:' . $stop_group;
      }
      if ($stop_tag != ""  ){
				if ( $in_tags == "" ) $in_tags = $stop_tag;
				else $in_tags = $in_tags . ',' . $stop_tag;
      }
      if ( $stop_instance == "" ) {
        $status_msg = shell_exec ('./i_inventory.py -C stop -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      } else {
        $status_msg = shell_exec ('./i_inventory.py -C stop -i ' . $stop_instance . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      }
      $status_array = split ("\n", $status_msg);
      updateInventory();
      break;

    case "start":
      $in_tags = "";
      if ($start_name != ""  ){
        $in_tags = 'Name:' . $start_name;
      }
      if ($start_group != ""  ){
				if ( $in_tags != "" ) $in_tags = $in_tags . ',' . 'Group:' . $start_group;
				else $in_tags = 'Group:' . $start_group;
      }
      if ($start_tag != ""  ){
				if ( $in_tags == "" ) $in_tags = $start_tag;
				else $in_tags = $in_tags . ',' . $start_tag;
      }
      if ( $start_instance == "" ) {
        $status_msg = shell_exec ('./i_inventory.py -C ' . 'start' . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      } else {
        $status_msg = shell_exec ('./i_inventory.py -C ' . 'start' . ' -i ' . $start_instance . ' -t "' . $in_tags . '" -k ' . $keyid . ' -s ' . $secret . ' 2>&1');
      }
      $status_array = split ("\n", $status_msg);
      updateInventory();
      break;

    case "update":
      updateInventory();
      break;

  } // switch

  get_version()

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AWS Web</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link type="text/css" rel="stylesheet" href="default.css"/>
<script type="text/javascript" src="default.js"></script>
<body>
<div id="page" style="padding:5px;clear:both;border:1px solid #B0B0B0; overflow:hidden; position: relative" ><!-- page -->
<div id="content" style="float:left;clear:both; width: 100%; border:0px solid red;position: relative"><!-- content -->

<form style="border: #D9D9D9 solid 0px" autocomplete="on" action="/" method="post" name="theform">

<!-- TOP -->
<!-- UPDATE PANEL -->
<div style="width:100%;float:left;clear:both;border:1px solid #D9D9D9;position:relative">
  <div style="float:left;clear:both">
    <p><label style="float: left; width: 100px; margin-right: 10px; text-align: left; font-weight: bold; clear: left;" >key:</label><input name="key" size="35" ></p>
    <p><label for="secret" style="float: left; width: 100px; margin-right: 10px; text-align: left; font-weight: bold; clear: left;" >secret:</label><input name="secret" type="password" size="35" ></p>
  </div>
  <div style="float:left;">
    <p><label style="float: left; width: 50px; margin-left: 20px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >User:</label><input name="in_user" size="15" ></p>
  </div>
  <div style="float:left;margin-left: 10px">
    <p><button style="vertical-align: bottom" type="submit" name="submit_btn" value="update" >Update Inventory</button></p>
  </div>
  <div style="float:right;margin-left: 10px">
    <p>Version: <?php echo $version; ?></p>
  </div>
</div>

<!-- MIDDLE -->
<div style="width:100%;float:left;clear:both;border:1px solid #D9D9D9;position:relative">
<!-- LAUNCH PANEL -->
<div style="float:left;clear:both;width:100%;height:80px;border:1px solid #D9D9D9;padding:5px;">
  <p style="float:left;width:100px"><button type="submit" name="submit_btn" value="launch" >Launch</button></p>
  <p style="float:left;"><label for="ip" style="float:left; width: 160px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Instances to launch:</label><input maxlength="2" size="2" name="qty" type="text" /></p>
  <div style="float:left;height:80px;width:280px;">
    <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Name:</label><input size="10" name="launch_name" type="text" /></p>
    <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Group:</label><input size="10" name="launch_group" type="text" /></p>
  </div>
  <p style="float:left"><label for="ip" style="float:left; width: 65px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Tags:</label><input style="float:left" size="10" name="launch_tag" type="text" /><label style="float:left;margin-left:5px;margin-right: 8px;width:170px;font-size:xx-small;">([key1:value1,key2:value2]) pairs)</label></p>
  <p style="float:left"><label style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >AMI:</label><input style="float:left;font-size:small" size="30" name="ami" list="ami_list" type="text" value="" /><datalist style="font-size:small" id="ami_list">
<?php
    foreach ($ami_list as $ami) {
      echo '<option value="' . $ami . '" >';
    }
?>
</datalist></p>
</div>

<!-- START PANEL -->
<div style="float:left;width:100%;clear:left;height:50px;border:1px solid #D9D9D9;padding:5px">
  <p style="float:left;width:50px"><button type="submit" name="submit_btn" value="start" >Start</button></p>
  <p style="float:left"><label style="float: left; width: 210px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Instance:</label><select id=instance name=start_instance value="">
  <option value=''> </option> 
  <?php
    foreach ($ids_array as $id) {
      if ( $id != '' ) {
        echo '<option value="' . $id . '">' . $id . '</option>';
      }
    }
  ?>
  </select></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Name:</label><input maxlength="10" size="10" name="start_name" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Group:</label><input maxlength="10" size="10" name="start_group" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Tags:</label><input maxlength="10" size="10" name="start_tag" type="text" /></p>
</div>

<!-- STOP PANEL -->
<div style="float:left;width:100%;clear:left;height:50px;border:1px solid #D9D9D9;padding:5px">
  <p style="float:left;width:50px"><button type="submit" name="submit_btn" value="stop" >Stop</button></p>
  <p style="float:left"><label style="float: left; width: 210px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Instance:</label><select id=instance name=stop_instance value="">
  <option value=''> </option> 
  <?php
    foreach ($ids_array as $id) {
      if ( $id != '' ) {
        echo '<option value="' . $id . '">' . $id . '</option>';
      }
    }
  ?>
  </select></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Name:</label><input maxlength="10" size="10" name="stop_name" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Group:</label><input maxlength="10" size="10" name="stop_group" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Tags:</label><input maxlength="10" size="10" name="stop_tag" type="text" /></p>
</div>

<!-- TERMINATE PANEL -->
<div style="float:left;width:100%;height:50px;border:1px solid #D9D9D9;padding:5px;">
  <p style="float:left;width:50px" ><button type="submit" name="submit_btn" value="terminate">Terminate</button></p>
  <p style="float:left" ><label style="float: left; width: 210px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Instance:</label><select id=instance name=terminate_instance value="U" >
  <option value=''> </option> 
  <?php
    foreach ($ids_array as $id) {
      if ( $id != '' ) {
        echo '<option value="' . $id . '">' . $id . '</option>';
      }
    }
  ?>
  </select></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Name:</label><input maxlength="10" size="10" name="terminate_name" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Group:</label><input maxlength="10" size="10" name="terminate_group" type="text" /></p>
  <p style="float:left"><label for="ip" style="float:left; width: 75px; margin-right: 10px; text-align: right; font-weight: bold; clear: left;" >Tags:</label><input maxlength="10" size="10" name="terminate_tag" type="text" /></p>

</div>

</form>

<!-- ------------- BOTTOM --------------------------- -->
<div style="width:100%;float:left;height:310px;clear:both;border:2px solid #D9D9D9;position:relative;">
<!-- INVENTORY Panel -->
<div style="overflow:auto;width:99%;float:left;height:200px;border:1px solid #B0B0B0;padding:5px">
        <p><label style="font-weight:bold">Inventory:</label></p>
        <div style="width:15%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">id</div>
        <div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">ip</div>
        <div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">state</div>
        <div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">user</div>
        <div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">Name</div>
        <div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">Group</div>
        <div style="width:33%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #EEEEE0;border:1px solid #787878 ">tags</div>

<!-- Format into field the line comming in, it is ";" separated -->
        <?php
          foreach ( $inventory_array as $line ) {
            $line_array = convert_2_array ($line);
            if ( !$line_array) {
              continue;
            }
            if ($line_array["TAGS"] == "" ) {
              $line_array["TAGS"] = "-";
            }
//            echo $debug_str . '<br>';
            if ( trim($line_array["id"]) == "" ) continue;
            echo '<div style="width:15%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["id"] . '</div>';
            if ($line_array["ip"] ) {
            echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["ip"] . '</div>';
            } else {
              echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">none</div>';
            }
            echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["state"] . '</div>';
            echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["user"] . '</div>';
            echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["Name"] . '</div>';
            echo '<div style="width:10%;float:left;font-size:small;text-align:center;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["Group"] . '</div>';
            echo '<div style="width:33%;float:left;font-size:small;text-align:left;vertical-align:middle;background-color: #D6D6D6;border:1px solid #787878 ">' . $line_array["TAGS"] . '</div>';
            echo "<br>";
//            echo $line . "<br>";
          }
        ?>
</div>

<!-- STATUS Panel -->
<div style="position:absolute;bottom:0;overflow:auto;width:99%;float:left;height:90px;border:1px solid #B0B0B0;padding:5px">
        <label style="font-weight:bold">Status:</label>
        <?php
          foreach ( $status_array as $line ) {
            echo $line . "<br>";
          }
          if ( $debug_str != "" ) {
            echo $debug_str . '<br';
          }
        ?>
</div>
</div>
</div><!-- content -->
</div><!-- page -->
</body></html>


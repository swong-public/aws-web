# Publicly accessible docker images

- aws-web
 - A simple web pannel into AWS
 - run it with docker run -it -p 80:80 registry.gitlab.com/swong-public/docker/aws-web:latest

1   This repo has CI/CD configured for gitlab. Pipeline is defined in .gitlab-ci.yml
    Basically src/build.sh script builds and push a new image out to gitlab docker registry
    and redeploy the docker application aws-web onto the deployer agent. 

2   gitlab runners agent are used to build and deploy base on changes in this repo.
    The gitlab-build-runner runs on the qemu instance  centos7-docker-builder.
    The gitlab-deploy-runner runs on the qemu instance  centos7-docker-deplooyer.

Some gitlab runner commands:
----------------------------
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
yum install gitlab-runner
usermod -a -G docker gitlab-runner  --- require for gitlab-runner to run docker commands
gitlab-runner register
gitlab-runner status
gitlab-runner list
gitlab-runner -h


Jenkins
-------
Jenkinsfile is used by jeckins pipeline.



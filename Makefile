

clean:	
			@echo clean docker with:
			@echo old images:
			@echo "  docker rmi -f \$$(docker images registry.gitlab.com/swong-public/docker/aws-web --filter "before=IMAGE_ID"  \
 | awk '{print \$$3;}' | grep -v IMAGE | xargs) "
			@echo
			@echo unused images:
			@echo "  docker rmi \$$(docker images -f \"dangling=true\" -q) " 
			@echo "  docker rmi \$$(docker images -a | grep none | awk '{ print \$$3\; }') "
			@echo
			@echo all containers:
			@echo "  docker rm -f \$$(docker ps -a -q); docker ps"
			@echo "  docker rm \$$(docker ps -aq -f status=exited) "


